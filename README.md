# HelloPrint Code Challenge - Multiplayer Game
## Frontend

### Instructions
The project needs NodeJs

1) Open a **terminal** in Mac
2) Check if you have installed NodeJS with **node -v** and then **npm -v** <br />
	- If Node is not installed, go to the following website -> https://nodejs.org/en/download/package-manager/
3) Go to the directory you wish to download the project
4) Clone the repository by copy and pasting in the terminal 
    **git clone https://gitlab.com/efsanchezus/helloprint_codechallenge_front.git**
5) Go to the project by typing **cd helloprint_codechallenge_front**
6) Type **npm install**
7) Run **npm run serve**
8) Go to the browser and type **http://localhost:8080**
9) Now it's time to install the backend. Go to **https://gitlab.com/efsanchezus/helloprint_codechallenge_back** 



### Structure of components
App <br/>
—> WrapperGame <br/>
	—> Configuration <br/>
		—> AmountOfPlayers<br/>
			—> Counter<br/>
	—> Game<br/>
		—> Banner<br/>
		—> Space<br/>
		—> Players<br/>
			—> BlackScreen	<br/>
			—> Player<br/>
				—> ErrorMessage<br/>
				—> Celebration or GameOver<br/>
				—> Description<br/>
				—> Space<br/>
				—> Input<br/>
				—> Space<br/>
				—> ButtonAnimation<br/>