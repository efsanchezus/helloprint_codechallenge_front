class Confetti{
	constructor(element, amount, size){
		this.element = element
		this.amount = amount
		this.size = size
		this.colors = ['#FF8E70', '#C76DFC', '#4192F6', '#77DDA8', '#F8E71C']
		this.active = false
		this.bindEvents()
	}
	bindEvents() {
		var element = document.querySelector(this.element);
		this.fire(element)
	}
	fire(element) {
		if(element.classList.length === 1) {
			var count = 0;
			element.classList.add('checked');
			let self = this
			var launch = setInterval(function() {
				if(count < self.amount){
					self.render(element);
					count++;
				}
				else {
					clearTimeout(launch);
				}
			}, 32);					
			this.active = true;
		}else{
		    element.classList.remove('checked');
		    this.active = false;
		}
	}

  	render(element) {
		var el = element;
		var c = this.newPiece();
		var s = this.size;
		var degs = 0;
		var x = 0;
		var y = 0;
		var opacity = 0;
		var count = 0;
		var xfactor;
		var yfactor = random(10,40)*(1 + s/10);
		if(random(0,1) === 1) {
			xfactor = random(5,40)*(1 + s/10);
			c.style.left = '-60px';
		}else{
			xfactor = random(-5,-40)*(1 + s/10);
			c.style.left = '60px';
		}
		var start = null;
		el.appendChild(c);
		var animate = function(timestamp){
			if(!start)
				start = timestamp;

			var progress = timestamp - start;
			if(progress < 2000) {
				window.requestAnimationFrame(animate);
			}else{
				el.removeChild(c);
			}
			c.style.opacity = opacity;
			c.style.webkitTransform = 'translate3d('+Math.cos(Math.PI / 36 * x)*xfactor+'px, '+Math.cos(Math.PI / 18 * y)*yfactor+'px, 0) rotateZ('+degs+'deg) rotateY('+degs+'deg)';
			degs += 15;
			x += 0.5;
			y += 0.5;
			if(count > 25)
				opacity -= 0.1;
			else
				opacity += 0.1;

			count++;
	    };
	    window.requestAnimationFrame(animate);
  	}	
	newPiece() {
		var newPiece = document.createElement('div');
		newPiece.style.width = 6+'px';
		newPiece.style.height = 8+'px';
		newPiece.style.position = 'absolute';
		newPiece.style.left = 0;
		newPiece.style.right = 0;
		newPiece.style.margin = '0 auto';
		newPiece.style.opacity = 0;
		newPiece.style.pointerEvents = 'none';
		newPiece.style.backgroundColor = this.colors[random(0, 4)];
		return newPiece;
	}
}

function random(min, max) {
  	return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default Confetti	